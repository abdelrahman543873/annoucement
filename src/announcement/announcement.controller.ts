import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AnnouncementService } from './announcement.service';
import { AddAnnouncementDto } from './input/add-announcement.dto';
import { AddAnnouncementViewDto } from './input/add-announcement-view.dto';

@Controller('announcement')
export class AnnouncementController {
  constructor(private readonly announcementService: AnnouncementService) {}

  @Post()
  async addAnnouncement(@Body() announcementDto: AddAnnouncementDto) {
    return await this.announcementService.addAnnouncement(announcementDto);
  }

  @Get('viewed/:userId')
  async getViewedAnnouncements(@Param('userId') userId: number) {
    return await this.announcementService.getViewedAnnouncements(userId);
  }

  @Get('unseen/:userId')
  async getUnseenAnnouncements(@Param('userId') userId: number) {
    return await this.announcementService.getUnseenAnnouncements(userId);
  }

  @Post('viewed')
  async addAnnouncementView(@Body() announcementDto: AddAnnouncementViewDto) {
    return await this.announcementService.addAnnouncementViewDto(
      announcementDto,
    );
  }

  @Get('generate')
  async getAnnouncement() {
    return await this.announcementService.generateAnnouncement();
  }
}
