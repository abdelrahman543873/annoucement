import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Announcement } from './announcement.entity';
import { LessThan, MoreThan, Repository } from 'typeorm';
import { AddAnnouncementDto } from './input/add-announcement.dto';
import { AnnouncementViewed } from './announcement-viewed.entity';
import { AddAnnouncementViewDto } from './input/add-announcement-view.dto';
import { faker } from '@faker-js/faker';

@Injectable()
export class AnnouncementService {
  constructor(
    @InjectRepository(Announcement)
    private readonly repository: Repository<Announcement>,
    @InjectRepository(AnnouncementViewed)
    private readonly announcementViewedRepository: Repository<AnnouncementViewed>,
  ) {}

  addAnnouncement(announcementDto: AddAnnouncementDto) {
    return this.repository.save(announcementDto);
  }

  generateAnnouncement() {
    return this.repository.save({
      title: faker.commerce.productDescription(),
    });
  }

  getViewedAnnouncements(userId: number) {
    return this.announcementViewedRepository.find({
      where: { user: { id: userId } },
      relations: ['announcement'],
    });
  }

  async getUnseenAnnouncements(userId: number) {
    const latestAnnouncement = await this.announcementViewedRepository.find({
      where: { user: { id: userId } },
      relations: ['announcement'],
      order: {
        user: { id: 'DESC' },
      },
    });
    const latestAnnouncementId = latestAnnouncement[0]?.announcement?.id;
    if (latestAnnouncementId)
      return await this.repository.find({
        where: {
          id: MoreThan(latestAnnouncementId),
        },
      });
    return await this.announcementViewedRepository.find({
      where: { user: { id: userId } },
      relations: ['announcement'],
    });
  }

  addAnnouncementViewDto(addAnnouncementViewDto: AddAnnouncementViewDto) {
    return this.announcementViewedRepository.save({
      announcement: { id: addAnnouncementViewDto.announcementId },
      user: { id: addAnnouncementViewDto.userId },
    });
  }
}
