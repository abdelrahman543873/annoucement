import { Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../user/user.entity';
import { Announcement } from './announcement.entity';

@Entity()
export class AnnouncementViewed {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @OneToOne(() => User)
  @JoinColumn()
  user: User;

  @OneToOne(() => Announcement)
  @JoinColumn()
  announcement: Announcement;
}
