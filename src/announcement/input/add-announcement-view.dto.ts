import { Type } from 'class-transformer';
import { IsInt } from 'class-validator';

export class AddAnnouncementViewDto {
  @IsInt()
  @Type(() => Number)
  announcementId: number;

  @IsInt()
  @Type(() => Number)
  userId: number;
}
