import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AnnouncementViewed } from './announcement-viewed.entity';

@Entity()
export class Announcement {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column()
  title: string;

  @ManyToMany(
    () => AnnouncementViewed,
    (announcementViewed) => announcementViewed.announcement,
  )
  announcementView: AnnouncementViewed;
}
