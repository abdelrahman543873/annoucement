import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AnnouncementModule } from './announcement/announcement.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mariadb',
      synchronize: true,
      host: 'localhost',
      database: 'earnbet',
      username: 'earnbet',
      password: 'earnbetPass',
      port: 3308,
      autoLoadEntities: true,
    }),
    UserModule,
    AnnouncementModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
