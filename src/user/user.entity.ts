import { AnnouncementViewed } from '../announcement/announcement-viewed.entity';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'user' })
export class User {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column({
    nullable: false,
    unique: true,
    length: 100,
    collation: 'utf8mb4_unicode_ci',
    transformer: {
      to: (value: string | null) => {
        return value ? value.trim() : value;
      },
      from: (value: string) => value,
    },
  })
  username: string;

  @Column({
    nullable: true,
    unique: false,
    length: 100,
    transformer: {
      to: (value: string | null) => {
        return value ? value.toLowerCase().trim() : value;
      },
      from: (value: string) => value,
    },
  })
  email: string;

  @ManyToMany(
    () => AnnouncementViewed,
    (announcementViewed) => announcementViewed.user,
  )
  announcementView: AnnouncementViewed;
}
